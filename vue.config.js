const path = require('path');
const webpack = require('webpack');
const fs = require('fs');

module.exports = {
    devServer: {
        overlay: {
          warnings: true,
          errors: true
        }
      },
    runtimeCompiler: true,

    configureWebpack: {
      resolve: {
        alias: {
          assets: path.resolve(__dirname, 'src/assets/'),
          utils: path.resolve(__dirname, 'src/utils/'),
          sc: path.resolve(__dirname, 'src/site_components/'),
          ac: path.resolve(__dirname, 'src/components/'),
        }
      },
      plugins: [
        new webpack.ProvidePlugin({
          $: 'jquery',
          jquery: 'jquery',
          'window.jQuery': 'jquery',
          jQuery: 'jquery'
        })
      ],
      devServer: {
        disableHostCheck: true
      }
    }
}