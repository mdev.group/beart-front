import axios from 'axios';

class API{
    constructor(baseURL) {
        this.baseURL = baseURL;
    }

    async get(url) {
        return (await axios.get(`${this.baseURL}${url}`)).data;
    }

    async put(url, data) {
        return (await axios.put(`${this.baseURL}${url}`, data)).data;
    }

    async post(url, data) {
        return (await axios.post(`${this.baseURL}${url}`, data)).data;
    }

    async delete(url) {
        return (await axios.delete(`${this.baseURL}${url}`)).data;
    }
}

const api = new API(`https://api.informalplace.ru/api/`);

export default api;