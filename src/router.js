import {createRouter, createWebHistory} from 'vue-router'

import HomePage from './views/Home'
import SomePage from './views/SomePage'

import Components from './views/Components/index'
import SpecificComponent from './views/Components/component'

import Pages from './views/Pages/index'
import SpecificPage from './views/Pages/page'

// 1. Define route components.
// These can be imported from other files
const About = { template: '<div>Alerts</div>' }

// 2. Define some routes
// Each route should map to a component.
// We'll talk about nested routes later.
const routes = [
  { path: '/admin', component: HomePage, meta: { title: "Главная", adminNav: true } },
  { path: '/admin/alerts', component: About, meta: { title: "Оповещения", adminNav: true } },
  { path: '/admin/dashboard', component: About, meta: { title: "Показатели", adminNav: true } },
  { path: '/admin/settings', component: About, meta: { title: "Общие настройки", adminNav: true } },
  { path: '/admin/users', component: About, meta: { title: "Пользователи", adminNav: true } },
  { path: '/admin/components', component: Components, meta: { title: "Компоненты", adminNav: true } },
  { path: '/admin/components/:component(.*)', component: SpecificComponent, meta: { title: "Компонент", adminNav: true } },
  { path: '/admin/pages', component: Pages, meta: { title: "Страницы", adminNav: true } },
  { path: '/admin/pages/:page(.*)', component: SpecificPage, meta: { title: "Конструктор страницы", adminNav: true } },
  { path: '/admin/tickets', component: About, meta: { title: "Заявки", adminNav: true } },


  { path: '/:page(.*)', component: SomePage }
]

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = createRouter({
  // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
  history: createWebHistory(),
  routes, // short for `routes: routes`
})

export default router;