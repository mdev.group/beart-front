import TextWithImage_1 from "./sections/TextWithImage_1";
import Hero_1 from "./sections/Hero_1";
import Navbar from "./sections/Navbar";

const sections = {
    TextWithImage_1,
    Hero_1,
    Navbar
}


export default {
    sections
}