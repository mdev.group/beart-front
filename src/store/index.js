import { createStore } from 'vuex'

import navigation from './modules/navigation'
import yandex from './modules/yandex'
import pages from './modules/pages'

// Create a new store instance.
const store = createStore({
  modules: {
    navigation,
    yandex,
    pages
  }
})


export default store;