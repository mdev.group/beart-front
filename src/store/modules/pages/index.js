import api from 'utils/api';

const navigation = {
  namespaced: true,
  state: () => ({
    list: [],
    isLoaded: false,
    saved: true
  }),
  mutations: {
    // sync state change. Without call to api. 
    SET_DATA(state, payload) {
      state.list = payload.list;
      state.isLoaded = true;
    },
    SET_PAGE_DATA(state, payload) {
      state.list[payload.id] = payload.data;
      state.isLoaded = true;
    },
    SET_SAVED(state, payload) {
      state.saved = payload;
    },
    REMOVE_PAGE(state, payload) {
      state.list.splice(payload.index, 1);
    }
  },
  actions: {
    // async state change. chaging state by call mutations
    async getData(context) {
      var res = await api.get(`pages`);

      context.commit('SET_DATA', {
        list: res
      });
    },

    async savePage(context, page){
      context.commit('SET_SAVED', false);
      console.log(page);
      await api.put(`pages/${page.page_slug}`, {
        page_data: page.page_data,
        page_structure: page.page_structure
      });

      context.commit('SET_SAVED', true);
    },

    async createPage(context, page){
      if(!page.page_slug || !page.page_url){
        return false
      }

      const newPage = await api.post(`pages`, {
        page_type: "static",
        page_slug: page.page_slug,
        page_url: page.page_url
      });

      context.commit('SET_PAGE_DATA', {
        data: newPage,
        id: context.state.list.length
      });
    },


    async deletePage(context, page){
      if(!page.page_slug){
        return false
      }

      await api.delete(`pages/${page.page_slug}`);

      context.commit('REMOVE_PAGE', {
        index: context.state.list.findIndex((item)=>item.slug == page.page_slug)
      });
    },
  },
  getters: {
    // complex getters. for example get list of user by specific filter
    getAllPages: (state) => {
      return state.list;
    },
    getPage: (state) => (path) => {
      return state.list.find(el => el.page_url == path);
    },
    getPageBySlug: (state) => (slug) => {
      return state.list.find(el => el.page_slug == slug);
    },
    isLoaded: (state) => {
      return state.isLoaded;
    }
  }
}

export default navigation;