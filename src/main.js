import { createApp } from 'vue'
import App from './App.vue'
import VueApexCharts from "vue3-apexcharts";

import router from './router'
import store from './store/index'

import VueTrumbowyg from 'vue-trumbowyg';
import 'trumbowyg/dist/ui/trumbowyg.css';

var app = createApp(App);
app.use(VueTrumbowyg);
app.use(router);
app.use(store);

app.use(VueApexCharts);

app.mount('#app')